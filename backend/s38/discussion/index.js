let http = require("http");

const port = 4000;

let users = [
		{
			"name": "Paolo",
			"email": "paolo@email.com"
		},
		{
			"name": "Shinji",
			"email": "shinji@email.com"
		}
	]

const app = http.createServer((request, response) => {

	if(request.url == '/items' && request.method == 'GET') {

		response.writeHead(200, {'Content-Type': 'text/plain'});

		response.end('Data retrieved from the database');
	}


	if(request.url == '/items' && request.method == 'POST') {

		response.writeHead(200, {'Content-Type': 'text/plain'});

		response.end('Data sent to the database');
	}

	//getting all items from mock database

	if(request.url == '/users' && request.method == 'GET') {

		response.writeHead(200, {'Content-Type': 'application/json'});

		response.end(JSON.stringify(users));
	}


	if(request.url == '/users' && request.method == 'POST') {
		let request_body = '';

		request.on('data', function(data){
			request_body += data;
		})

		request.on('end', () => {
			console.log(typeof request_body);

			let new_user = {
				"name": request_body.name,
				"email": request_body/email
			}

			users.push(new_user);

			response.end(JSON.stringify(users));
		})

		
	}

})

app.listen(port, () => console.log(`Server is running at localhost ${port}`));
