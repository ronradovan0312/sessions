let http = require("http");

const port = 4000;
const app = http.createServer((req, res) => {

	if(req.url == '/greeting') {

		res.writeHead(200, { 'Content-Type': 'text/plain'});

		res.end('Hello Again');

	} else if(req.url == '/homepage') {

		res.writeHead(200, { 'Content-Type': 'text/plain'});

		res.end('Welcome to the homepage.');

	// all other routes that are not included in if else-if
	} else {

		res.writeHead(200, { 'Content-Type': 'text/plain'});

		res.end('Page not available na po!');
	}
})

app.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);