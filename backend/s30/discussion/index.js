console.log("ES6 Updates");


// Exponent Operator
	const firstNum = 8 ** 2;
	console.log(firstNum);

	const secondNum = Math.pow(8,2);
	console.log(secondNum);

// Template Literals - allows us to write strings w/o concatination. Greatly helps us with code readability.

	let name = 'ken';

// using concatination
	let message = 'hello ' + name + '! Welcome to Programming';
	console.log('Message w/o template literals: ' + message);

// using template literals
// backsticks (``) & ${} for including javascript expressions

	message = `Hello ${name}! Welcome to Programming`;
	console.log(`Message with template literals: ${message}`)

// creates multi-line using template literals
	const anotherMessage = `
	${name} attended a Math Competition.
	He won by solving the problem 8 ** 2 with the answer of ${firstNum}`;
	console.log(anotherMessage);

	const interstRate = .1;
	const principal = 1000;

	console.log(`The interest on your savings amount is: ${principal * interstRate}`);

// Array indeces
	const fullName = ['Juan', 'Dela', 'Cruz'];
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you. `)

// using array destructuring
	const [firstName, middleName, lastName] = fullName;
	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

// Object destructuring
	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	}
// using dot notation
	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's nice to meet you.`);

// using Object destructuring
	const { maidenName, givenName, familyName} = person;
	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's nice to meet you.`)

// using object destructuring in function
	function getFullName ({givenName,maidenName,familyName}) {
		console.log(`${givenName} ${maidenName} ${familyName}`)
	};
	getFullName(person);

// Arrow function
	const hello = () => {
		console.log('Hello World!')
	};
	hello();

// Traditional Function w/o template literals
	/*function getFullName(fName, mName, lName) {
		console.log(fName +' '+ mName +' '+ lName);
	}
	console.log('John', 'D', 'Smith');
*/

// Arrow function with template literals
	const printFullName = (fname, mName, lName) => {
		console.log(`${fname} ${mName} ${lName}`)
	}
	printFullName('Jane', 'Dela', 'Cruz');

// Arrow function with loops
	const students = ['John', 'Jane', 'Judy'];

// Traditional Function
	students.forEach (function(students){
		console.log(`${students} is a students.`)
	})

// Arrow Function
	students.forEach((students) =>{
		console.log(`${students} is a students.`)
	})

// Implicit Return Statements

// Traditional Function
	/*function add(x, y){
		return x + y;
	} 
	let total = add(5,2);
	console.log(total);*/

// Arrow function
// single line arrow function
	const add = (x,y) => x +y;
	let total = add(2,5);
	console.log(total);

// Default Argument Function
	const greet = (name = 'user') => {
		return `Good afternoon ${name}`
	}
	console.log(greet());
	console.log(greet('Judy'));

	