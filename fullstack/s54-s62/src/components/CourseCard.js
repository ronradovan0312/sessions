import PropTypes from 'prop-types';
import { useState } from 'react';
import {Card, Button} from 'react-bootstrap';

export default function CourseCard({course}) {

// Destructuring the contents of 'course'
	const {name, description, price} = course;

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll(){
		if(seats > 0){
			setCount(prev_value => prev_value + 1);
			setSeats(prev_value => prev_value - 1);
		} else {
			alert("No more seats available.");
		}
		
	}

	


	return(
		<Card>
			<Card.Body className="CourseCard">
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP{price}</Card.Text>

				<Card.Text>Enrollees: </Card.Text>
				<Card.Text>{count}</Card.Text>

				<Card.Text>Seats</Card.Text>
				<Card.Text>{seats}</Card.Text>

				<Button variant="primary" onClick={enroll}>Enroll Now!</Button>
			</Card.Body>
		</Card>
	)
}


// PropTypes is used for validating the data from the props
 CourseCard.propTypes = {
 	course: PropTypes.shape({
 		name: PropTypes.string.isRequired,
 		description: PropTypes.string.isRequired,
 		price: PropTypes.number.isRequired
 	})
 }