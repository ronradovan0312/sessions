// console.log('Hello World!');

/*{
	"city": "Pateros",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// [SECTION] JSON Arrays
// 	"cities": [
// 		{
// 			"city": "Quezon City",
// 			"province": "Metro Manila",
// 			"country": "Philippines"
// 		},

// 		{
// 			"city": "Batangas",
// 			"province": "Batangas",
// 			"country": "Philippines"
// 		},

// 		{
// 			"city": "Star City",
// 			"province": "Pasay",
// 			"country": "Philippines"
// 			"rides": [
// 				{
// 					"name": "Star Flyer"
// 				},
// 				{
// 					"name": "Gabi ng Lagim"
// 				}
// 			]
// 		}
// ]		


// [SECTION] JSON Methods
	let zuitt_batches = [
		{batchName: "303"},
		{batchName: "207"}
	]	

// Before stringification , javascript read the variable as a regular JS Array
console.log(`Output before stringification: `);
console.log(zuitt_batches);

// After the JSON.stringify function, JS now reads the variable as a string (equivalent to converting array into JSON format.)
console.log(`Output after stringification: `);
console.log(JSON.stringify(zuitt_batches));


// User Details
	let first_name = prompt('What is your first name:?');
	let last_name = prompt('What is your last name:?');

	let other_data = JSON.stringify({
		firstName: first_name,
		lastName: last_name
	})
console.log(other_data);


// [SECTION] Convert Stringified JSON into Javascipt Objects/Arrays
	let other_data_JSON = `[{"firstName": "Ron", "lastName": "Radovan"}]`;

// The parse function/method converts the JSON string into a JS Object/Array
	let parsed_other_data = JSON.parse(other_data_JSON);

	console.log(parsed_other_data);

// Since the JSON is converted, you can now access the properties in regular javacript function
	console.log(parsed_other_data[0].firstName);