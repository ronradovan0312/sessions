// console.log("hello world")

// creating objects using initializers/objects literals.
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using intializers/object literals");
console.log(cellphone);
console.log(typeof cellphone);

// creating objects using a constructor function.
function Laptop(name, manufactureDate) {
	this.name = name; 
	this.manufactureDate = manufactureDate;
}

// multiple instance of an object using the "new" keyword.
// this method is call instantiation.
let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using constructor function");
console.log(laptop);

let laptop2 = new Laptop('Macbook Air', 2020);
console.log("Result from creating objects using constructor function");
console.log(laptop2);

// Accessing Object Properties

// using square bracket notation
console.log('Result form square bracket notation: ' + laptop2['name']);

// using dot notation
console.log('Result from dot notation ' + laptop2.name);

// access array objects.

let array = [laptop, laptop2];

console.log(array[0]['name']);
console.log(array[0].name);

// [SECTION] Adding/Deleting/Reassigning Objects Properties.

// empty object
let car = {};

// adding object properties dot notation
// objjectName.Key = 'value'
car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation');
console.log(car);

// adding object properties using square bracket notation

car['manufacturing date'] = 2019;
console.log(car['manufacturing date']);
console.log(car['Manufacturing Date']);
// we cannot access the object property using dot notation if the key has spaces
// console.log(car.Manufacturing date);
console.log("Result from adding properties using the square bracket notation: ");
console.log(car);

// deleting object properties 

delete car['manufacturing date'];
// delete car.manufactureDate;
console.log('Result from deleting properties: ');
console.log(car);

// Reassigning Object Properties
car.Name = 'Honda Civic Type R';
console.log('Result from reassigning properties: ');
console.log(car);

// [SECTION] Object Methods - a methods  is a function which acts as a property of an object.

let person = {
	name: 'Barbie',
	greet: function(){
		console.log('Hello! my name is ' + this.name);
	}
}

console.log(person);
console.log('Result from object methods: ');
person.greet();

// adding methods to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward');
}
console.log(person);
person.walk();

let friend = {
	name: "Ken",
	address: {
		city: "Austin",
		state: "Texas",
		country: "USA"
	},
	email: ['ken@gmail.com', 'ken@mail.com'],
	introduce: function(person) {
		console.log('Nice to meet you ' + person.name + ' i am ' + this.name + ' from ' + this.address.city + ' ' + this.address.state + " " + this.address.country);
	}
}
friend.introduce(person);