// console.log("hello world");

	let grades = [98.5, 94.3, 89.2, 90.1];
	let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu", "Lenovo"];
	let mixed_array = [12, "Asus", null, undefined, {}];

// Alternative way to write arrays

	let my_tasks = [
			"drink html",
			"eat javascript",
			"inhale CSS",
			"bake sass"
		];

// Reassigning values

	console.log("Array before reassignment ");
	console.log(my_tasks);

	my_tasks[0] = "run hello world!";
	console.log("Array after the reassignment.");
	console.log(my_tasks);

// Getting the length of an array

	console.log(grades[3]);
	console.log(computer_brands.length);

// Accessing last element in an array

	let index_of_last_element = computer_brands.length - 1;

	console.log(computer_brands[index_of_last_element]);

// [SECTION] Array methods

	let fruits = ["Apple", "Orange", "Kiwi", "Passionfruit"];


// Push Methods
	console.log("Current array: ");
	console.log(fruits);

	fruits.push("Mango", "Cocomelon");

	console.log("Updated array after push method");
	console.log(fruits);

// Pop Method
	console.log("Current array: ");
	console.log(fruits);

	let remove_item = fruits.pop();

	console.log("Updated array after pop method");
	console.log(fruits);
	console.log("Removed fruit " + remove_item);

// Unshift Method - Adding items in the beginning of the array
	console.log("Current array: ");
	console.log(fruits);

	fruits.unshift("Lime", "Star Apple");

	console.log("Updated array after unshift method");
	console.log(fruits);
	
// Shift Method - Remove item at the beginning of the array
	console.log("Current array: ");
	console.log(fruits);

	fruits.shift();

	console.log("Updated array after shift method");
	console.log(fruits);

// Splice Method - Can simultaneously update multiple items in an array starting form a specified index.
	console.log("Current array: ");
	console.log(fruits);

// splice(startingIndex, numberOfItemsToBeDeleted, itemsToBeDeleted)
	fruits.splice(1, 2, 'Lime', 'Cherry');

	console.log("Updated array after splice method");
	console.log(fruits);

// Sort Method Can sort items in an array alphanumerically, either in ascending or descending
	console.log("Current array: ");
	console.log(fruits);


	fruits.sort();

	console.log("Updated array after sort method");
	console.log(fruits);

// [Sub-section] Non-Mutator Methods 

// indexOf Method - hets the index of a specific items.
	let index_of_lenovo = computer_brands.indexOf("Lenovo");
	console.log("The index of lenovo is: " + index_of_lenovo);

	let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
	console.log("The index of lenovo starting from the end of the array is: " + index_of_lenovo_from_last_item);


// Slice Method
	let hobbies = ["Gaming", "Running", "Cheating", "Cycling", "Writing"];

	let sliced_array_from_hobbies = hobbies.slice(2);
	console.log(hobbies);
	console.log(sliced_array_from_hobbies);

	let sliced_array_from_hobbies_B = hobbies.slice(2, 3);
	console.log(hobbies);
	console.log(sliced_array_from_hobbies_B);

	let sliced_array_from_hobbies_C = hobbies.slice(-3);
	console.log(hobbies);
	console.log(sliced_array_from_hobbies_C);

// toString Method
	let string_array = hobbies.toString();
	console.log(string_array);

// Concat Method 
	let greeting = ["Hello", "World"];
	let exclamation = ["!", "?"]; 

	let concat_greeting = greeting.concat(exclamation); 
	console.log(concat_greeting);

// Join Method - This will convert the array to a string AND insert a specified seperator between them. The separtor is defined as the argument of the join() method.
	console.log(hobbies.join(' - '));

// foreach Method
	hobbies.forEach(function(hobbies){
		console.log(hobbies);
	});

// Map Method - Loops through the whole array and adds each item to a new array.
	let numbers_list = [1, 2, 3, 4, 5];
	let numbers_map = numbers_list.map(function(number){
		return number * 2;
	});
	console.log(numbers_map);

// Filter Method
	let filtered_numbers = numbers_list.filter(function(number){
		return (number < 3);
	});
	console.log(filtered_numbers);


// [SECTION] Multi-Dimensional Arrays

	let chess_board = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
	console.log(chess_board[1][4]);

	