// using  the aggregate method
	db.fruits.aggregate([
// $match - used to match or get documents that satisfies the condition
// $match is similar to find(). You can use query operators to make your criteria more flexible.
  		{ $match: { onSale: true }},

// group - allows us to group together documents and create an analysis out of the group elements.
// _id: $supplier_id
  		{ $group: { _id: "$supplier_id", total: { $sum: "$stock"}}}
	]);


	db.fruits.aggregate([
			{ $match: { onSale: true}},
			{ $group: { _id: '$supplier_id', avgStocks: { $avg: '$stock'}}},
			{ $project: { _id: 0}}
		]);


	db.fruits.aggregate([
			{ $match: { onSale: true}},
			{ $group: { _id: '$supplier_id', maxPrice: { $max: '$price'}}},
			{ $sort: { maxPrice: -1}}
		]);

	db.fruits.aggregate([
			{$unwind: "$origin"}
		]);

	db.fruits.aggregate([
			{ $unwind: '$origin'},
			{ $group: {_id: '$origin', kinds: { $sum: 1}}}
		]);