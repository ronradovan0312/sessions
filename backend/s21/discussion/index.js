// // Function Declaration & Invocation
// function printName(){
// 	console.log("My Name is Jeff");
// }
// printName();

// // Function Expression
// let variable_function = functon() {
// 	console.log("Hello from function expression!");
// }

// variable_function();

// Scoping
let global_variable = "Call me mr. Worldwide";
console.log(global_variable);

function showNames(){
	let function_varaible = "joe";
	const function_const = "john";

	console.log(function_varaible);
	console.log(function_const);
	console.log(global_variable);
}

showNames();

// NESTED FUNCTIONS
function parentFunction(){
	let name = "jane";

	function childFunction(){
		let nested_name = "John";
		console.log(name);
	}
	childFunction();
}
parentFunction();

// Best Practice for Function Naming
function printWelcomeMessageForUser(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello," + firstName + " " + lastName + "!");
	console.log("Welcome sa page ko!");
}
printWelcomeMessageForUser();

// Return Statement
function fullName(){
	return "Ronnel Radovan";
}
console.log(fullName());