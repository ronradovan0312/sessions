// Arguments and Parameters

	function printName(name) {
		console.log("I'm the real " + name)
	}
printName("Slim Shady");

	function checkDivisibilityBy2(number) {
		let result = number % 2;

		console.log("The remainder of " + number + " is " + result);
	}
checkDivisibilityBy2(15);

// Multiple Arguments and Parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);		
	}
	createFullName("Juan", "Dela", "Cruz");

// Usage of Prompt and Alert

	let user_name = prompt("Enter your username: ");

	function displayWelcomeMessageForUser(userName) {
		alert("Welcome back to Valorant " + userName);
	}

	displayWelcomeMessageForUser(user_name);

	