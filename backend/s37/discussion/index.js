// use 'require' derective to load the Node.js modules
// a module is a software component or part of a program that contains one or more routines
// the "http" module lets Node.js transfer data using the HYPER TEXT TRANSFER PROTOCOL
let http = require("http");

// createServer() method creates an HTTP server that listens to request on a specific port and gives response back to the client.
// 4000 is the port where the server will listen to. A port is a virtual point wheree the network connecetion start and end.
http.createServer(function(request, response) {

// 200 - status code for the response - means OK
// sets the content-type of theresponse to be a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

//  send the response with the text content 'Hello World'
	response.end('Hello World!');

}).listen(4000)

// When server is running, console will print the messag:
console.log('Server is running at port 4000')