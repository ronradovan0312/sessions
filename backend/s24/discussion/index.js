// // [SECTION] While loop

// 	let count = 5;

// 	while (count !== 0) {
// 		console.log("Current value of count: " + count);
// 		count--;
// 	}

// // [SECTION] Do While Loop

// 	let number = Number(prompt("Give me a number: "));

// 	do {
// 		console.log("Current value of number: " + number);

// 		number += 1;
// 	} while (number <= 10 );

// // [SELECTION] For Loop


// 	for (let count = 0; count <= 20; count++) {
// 		console.log("Current for loop value: " + count);
// 	}

	
// 	let my_string = "Ron";

// 	console.log(my_string.length);

// // to get a specific letter in a string
// 	console.log(my_string[2]);

// //  Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
// 	for (let index = 0; index < my_string.length; index++) {
// 		console.log(my_string[index]);
// 	}

 // let my_name = "Ronnel Radovan";

 //   for(let index = 0; index < my_name.length; index++){
 //   	if(my_name[index] != 'a' && my_name[index] != 'e' && my_name[index] != 'i' && my_name[index] != 'o' && my_name[index] != 'u'){
 //   		console.log(my_name[index]);
 //   	}
 //   }
    

  let name_two = "rafael"
  
  for( let index = 0; index < name_two; index++) {
  	if(name_two[index].toLowerCase() == "a") {
  		console.log("skipping...");
  		continue;
  	}
  	if(name_two[index] == "e") {
  		break;
  	}
  	console.log(name_two[index]);
  }

